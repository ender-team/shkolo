package window;

public class ProfileRequest {
    private Profile profile;
    private boolean logged;
    public ProfileRequest(Profile profile, boolean logged) {
        this.profile = profile;
        this.logged = logged;
    }

    public Profile getProfile() {
        return this.profile;
    }
    public boolean isLogged() {
        return this.logged;
    }
}

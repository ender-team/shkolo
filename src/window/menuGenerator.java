package window;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

public class menuGenerator {
    public static Menu fileMenu() {
        Menu fileMenu = new Menu("File");
        fileMenu.getItems().add(new MenuItem("New School"));
        fileMenu.getItems().add(new MenuItem("Open School"));
        fileMenu.getItems().add(new SeparatorMenuItem());
        fileMenu.getItems().add(new MenuItem("Settings"));
        fileMenu.getItems().add(new SeparatorMenuItem());
        fileMenu.getItems().add(new MenuItem("Exit"));
        return fileMenu;
    }
}

package window;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class addWindowGenerator {
    public static String makeScreen(String title, String labelText) {
        Stage window = new Stage();
        window.setTitle(title);

        Label label = new Label(labelText);
        TextField textField = new TextField();
        Button button = new Button("ok");

        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll(label, textField, button);

        Scene scene = new Scene(stackPane);
        window.setScene(scene);
        window.showAndWait();
        return textField.getText();
    }
}

package window;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.LinkedList;

public class LoginHandler {

    static ProfileRequest request;
    static Profile profile;

    public static Profile login(LinkedList<Profile> profiles) {
        Stage window = new Stage();
        window.setTitle("Login");

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);
        grid.setAlignment(Pos.CENTER);

        Button logInButton = new Button("Log in");
        GridPane.setConstraints(logInButton, 0, 0);
        logInButton.setOnAction(e -> {
            ProfileRequest request = Login.show(profiles);
            if (request.isLogged()) {
                LoginHandler.request = request;
                window.close();
            } else {
                // TODO: Error window
            }
        });

        Button registerButton = new Button("Register");
        GridPane.setConstraints(registerButton, 0, 1);
        registerButton.setOnAction(e -> {
            Profile profile = Register.show();
            profiles.add(profile);
        });

        grid.getChildren().addAll(logInButton, registerButton);
        Scene scene = new Scene(grid, 220, 260);
        window.setScene(scene);
        window.showAndWait();
        return profile;
    }
}
package window;

import javafx.scene.control.*;
import school.*;

import java.util.Map;

public class treeViewGenerator extends TreeCell<String> {

    public static TreeView<String> show(School school) {
        TreeItem<String> root = new TreeItem<>("Jonkata");
        root.setExpanded(true);

        TreeItem<String> klass = makeBranch("Classes", root);
        for (Map.Entry<Integer, Klass> klassEntry : school.getKlasses().entrySet()) {
            TreeItem<String> division = makeBranch(Integer.toString(klassEntry.getKey()), klass);

            ContextMenu divisionMenu = new ContextMenu();
            MenuItem addDivisionItem = new MenuItem("Add klass");
            divisionMenu.getItems().add(addDivisionItem);
            addDivisionItem.setOnAction(e -> {
                String newName = addWindowGenerator.makeScreen("New Klass", "Enter the name of the new Klass");
                makeBranch(newName, klass);
            });

            for (Map.Entry<Character, Division> divisionEntry : klassEntry.getValue().getDivisions().entrySet()) {
                TreeItem<String> student = makeBranch(Character.toString(divisionEntry.getKey()), division);
                for (Student studentEntry : divisionEntry.getValue().getStudents()) {
                    makeBranch(studentEntry.getName(), student);
                }
            }
        }
        TreeItem<String> teacher = makeBranch("Teachers", root);
        for (Teacher teacherEntry : school.getTeachers()) {
            makeBranch(teacherEntry.getName(), teacher);
        }
        TreeItem<String> subject = makeBranch("Subjects", root);
        for (Subject subjectEntry : school.getSubjects()) {
            makeBranch(subjectEntry.getName(), subject);
        }
        TreeView<String> treeView = new TreeView<>(root);treeView.setShowRoot(true);return treeView;
    }

    private static TreeItem<String> makeBranch(String name, TreeItem<String> parent) {
        TreeItem<String> item = new TreeItem<>(name);
        item.setExpanded(false);
        parent.getChildren().add(item);
        return item;
    }
}

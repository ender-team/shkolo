package window;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Register {
    public static Profile show() {
        Stage window = new Stage();
        window.setTitle("Register");

        // Create gridPane
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);

        // Username label
        Label nameLabel = new Label("Username:");
        GridPane.setConstraints(nameLabel, 0, 0);

        // Username field
        TextField nameField = new TextField();
        nameField.setPromptText("username");
        GridPane.setConstraints(nameField, 1, 0);

        // Email label
        Label emailLabel = new Label("Email:");
        GridPane.setConstraints(emailLabel, 0, 1);

        // Email field
        TextField emailField = new TextField();
        emailField.setPromptText("email ");
        GridPane.setConstraints(emailField, 1, 1);

        // Password label
        Label passwordLabel = new Label("Password:");
        GridPane.setConstraints(passwordLabel, 0, 2);

        // Password field
        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("password");
        GridPane.setConstraints(passwordField, 1, 2);

        // Register button
        Button register = new Button("Register");
        GridPane.setConstraints(register, 1, 4);
        register.setOnAction(e -> {
            window.close();
        });

        // Generate scene
        grid.getChildren().addAll(nameLabel, nameField, emailLabel, emailField, passwordLabel, passwordField, register);
        Scene scene = new Scene(grid);

        // Show screen
        window.initModality(Modality.APPLICATION_MODAL);
        window.setScene(scene);
        window.setWidth(340);
        window.setMinWidth(340);
        window.showAndWait();

        return new Profile(nameField.getText(), emailField.getText(), passwordField.getText());
    }
}

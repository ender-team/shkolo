package window;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.LinkedList;

public class Login {

    static ProfileRequest request;

    public static ProfileRequest show(LinkedList<Profile> profiles) {
        Stage window = new Stage();
        window.setTitle("Log in");

        // Create gridPane
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);

        // Email label
        Label emailLabel = new Label("Email:");
        GridPane.setConstraints(emailLabel, 0, 0);

        // Email field
        TextField emailField = new TextField();
        emailField.setPromptText("email");
        GridPane.setConstraints(emailField, 1, 0);

        // Password label
        Label passwordLabel = new Label("Password:");
        GridPane.setConstraints(passwordLabel, 0, 1);

        // Password field
        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("password");
        GridPane.setConstraints(passwordField, 1, 1);

        // Login button
        Button loginButton = new Button("Log in");
        GridPane.setConstraints(loginButton, 1, 3);

        // Generate scene
        grid.getChildren().addAll(emailLabel, emailField, passwordLabel, passwordField, loginButton);
        Scene scene = new Scene(grid);

        loginButton.setOnAction(e -> {
            Profile searched = null;
            boolean logged = false;
            Label errorLabel = new Label();
            if (passwordField.getText().length() < 4) {
                errorLabel.setText("Enter more than 4 symbols for password");
            }
            for (Profile profile : profiles) {
                if (profile.getEmail().equals(emailField.getText())) {
                    searched = profile;
                    if (profile.checkPassword(passwordField.getText()))
                        logged = true;
                    break;
                }
            }
            request = new ProfileRequest(searched, logged);
            window.close();
        });

        // Show screen
        window.initModality(Modality.APPLICATION_MODAL);
        window.setScene(scene);
        window.setWidth(340);
        window.setMinWidth(340);
        window.showAndWait();

        return request;
    }
}

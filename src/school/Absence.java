package school;

public class Absence implements java.io.Serializable {
    private int[] date;
    private String type;
    private boolean excuse;
    private String subject;
    public  Absence(int[] date, String type, String subject) {
        this.date=date;
        this.type=type;
        this.excuse=false;
        this.subject=subject;
    }

    public void setDate(int[]date ){
        this.date=date;
    }

    public int[] getDate(){
            return this.date;
    }

    public void setType(String type ){
        this.type=type;
    }

    public String getType(){
        return this.type;
    }

    public void setSubject(String subject ){
        this.subject=subject;
    }

    public String getSubject(){
        return this.subject;
    }

    public boolean isExcused() {
        return excuse;
    }

    public void excuse() {
        this.excuse=true;
    }
}

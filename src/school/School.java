package school;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

public class School implements java.io.Serializable {
    private String name;
    private LinkedHashMap<Integer, Klass> klasses = new LinkedHashMap<>();
    private LinkedList<Teacher> teachers = new LinkedList<>();
    private LinkedList<Subject> subjects = new LinkedList<>();

    public School(String name) {
        this.name = name;
    }

    public LinkedHashMap<Integer, Klass> getKlasses() {
        return this.klasses;
    }
    public Klass getKlass(int number) {
        if (klasses.size() == 0) return null;
        return klasses.get(number);
    }
    public void createKlass(int number) {
        klasses.put(number, new Klass());
    }

    public LinkedList<Teacher> getTeachers() {
        return this.teachers;
    }
    public Teacher getTeacher(String name) {
        Teacher teacher=new Teacher();
        for (int i = 0; i < teachers.size(); i++) {
            if (teachers.get(i).getName().equals(name)) {
                teacher = teachers.get(i);
                break;
            }
        }
        return teacher;
    }
    public void addTeacher(String name, String subject) {
        teachers.add(new Teacher(name, subject));

    }
    public void fireTeacher(String name) {
        for(int i=0; i<teachers.size(); i++){
            if(teachers.get(i).getName().equals(name)){
                teachers.remove(i);
                break;
            }
        }
    }

    public String getName() {
        return this.name;
    }

    public LinkedList<Subject> getSubjects() {
        return this.subjects;
    }

    //TODO: METHODS WITH SUBJECTS


    public void getStudentsByMark() {
        LinkedList<Student> students=new LinkedList<>();
        for (Map.Entry<Integer, Klass> klassEntry : klasses.entrySet()) {
            for(Map.Entry<Character, Division> divisionEntry : klassEntry.getValue().getDivisions().entrySet()) {
                students.addAll(divisionEntry.getValue().getStudents());
            }
        }
        for (int i = 0; i < students.size() - 1; i++) {
            for (int j = 0; j < students.size() - i - 1; j++) {
                if (students.get(j).getAverageMark() > students.get(j + 1).getAverageMark()) {
                    // swap arr[j+1] and arr[i]
                    Student temp = students.get(j);
                    students.set(j, students.get(j + 1));
                    students.set(j, temp);
                }
            }
        }
    }
}

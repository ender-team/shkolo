package school;

import java.util.LinkedList;

public class Division implements java.io.Serializable {
    private LinkedList<Student> students = new LinkedList<>();

    public LinkedList<Student> getStudents() { return this.students; }
    public Student getStudent(String name) {
        for (Student student : students) {
            if (student.getName().equals(name)) return student;
        }
        return null;
    }

    public void addStudent(String name) { students.add(new Student(name)); }

    public boolean expelStudent(String name) {
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getName().equals(name)) {
                students.remove(i);
                return true;
            }
        }
        return false;
    }
}

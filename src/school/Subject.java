package school;

import java.util.LinkedList;

public class Subject implements java.io.Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package school;

import school.Person;

import java.util.LinkedHashMap;
import java.util.LinkedList;

public class Teacher extends Person implements java.io.Serializable {
    private String name;
    private String subject;
    private LinkedList<String> teacherOf;

    public LinkedList<String> getTeacherOf() {
        return teacherOf;
    }

    public void addTeacherTo(int klass, char division) {
        teacherOf.add(String.valueOf(klass + division));
    }

    public void removeTeacherTo(int klass, char division) {
        teacherOf.remove(String.valueOf(klass + division));
    }

    public Teacher() {
        name = "";
        subject = "";
    }

    public Teacher(String name, String subject) {
        this.name = name;
        this.subject = subject;
        //TODO: CREATE
    }

    public String getName() {
        return this.name;
    }

    // TODO: setters and getters
}

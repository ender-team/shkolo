package school;

public class Mark implements java.io.Serializable {
    private double mark;
    private String subject;
    public Mark(double mark, String subject) {
        this.mark=mark;
        this.subject=subject;
    }

    public double getMark(){
        return this.mark;
    }

    public String getSubject(){
        return this.subject;
    }

    public void edit(double newMark) {
        this.mark=newMark;
    }
}

package school;

public class Person implements java.io.Serializable {
    private String name;
    private String email;

    public String getName() { return name; }
    public void SetName() { this.name = name; }

    public String getEmail() { return email; }
    public void SetEmail() { this.email = email; }
}

package school;

import java.util.LinkedList;

public class Student implements java.io.Serializable {
    private String name;
    private LinkedList<Mark> marks;
    private LinkedList<Absence> absences;
    public Student(String name) {
        this.name = name;
        this.marks = new LinkedList<>();
        this.absences = new LinkedList<>();
    }

    public String getName() { return this.name; }
    public void setName(String name) { this.name = name; }

    public Mark getMark(int index) { return this.marks.get(index); }
    public void setMark(int index, Mark mark) { this.marks.add(index, mark); }

    public Absence getAbsence(int index) { return this.absences.get(index); }
    public void setAbsence(int index, Absence absence) { this.absences.add(index, absence); }

    public double getAverageMark() {
        double average = 0;

        for (int i = 0; i < marks.size(); i++) {
            average += marks.get(i).getMark();
        }
        return average / marks.size();
    }
}

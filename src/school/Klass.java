package school;

import school.Division;

import java.util.LinkedHashMap;

public class Klass implements java.io.Serializable {
    private LinkedHashMap<Character, Division> divisions = new LinkedHashMap<>();

    public LinkedHashMap<Character, Division> getDivisions() { return this.divisions; }

    public Division getDivision(char letter) {
        if (divisions.size() == 0) return null;
        return divisions.get(letter);
    }

    public void createDivision(char letter) { divisions.put(letter, new Division()); }
}

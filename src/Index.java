import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TreeView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import school.*;
import window.*;

import java.io.*;
import java.util.LinkedList;

public class Index extends Application {

    LinkedList<Profile> profiles = new LinkedList<>();
    private static School school;
    private static Profile currentUser;

    Stage window;

    private static String path = System.getProperty("user.home") + "\\Documents\\shkolo.sav";
    public static void main(String[] args) {
//        readData();
        school = new School("Jonkata");
        school.createKlass(7);
        school.getKlass(7).createDivision('b');
        school.getKlass(7).getDivision('b').addStudent("Rosen Rusev");
        school.getKlass(7).getDivision('b').addStudent("Ico");
        school.addTeacher("Taseva", "BG");
        currentUser = new Profile("rosen1000", "rosen@gmail.com", "1234");
        launch(args);
//        writeData(school);
    }

    @Override
    public void start(Stage stage) {
        window = stage;
        window.setTitle("Shkolo");

        Profile request = LoginHandler.login(profiles);

        TreeView<String> treeView = treeViewGenerator.show(school);

        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(treeView);

        Menu fileMenu = menuGenerator.fileMenu();
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().add(fileMenu);
        borderPane.setTop(menuBar);

        Scene scene = new Scene(borderPane, 1300, 650);
        window.setScene(scene);
        window.show();
//        if (!request.isLogged()) {
//            System.out.println("Wrong password");
//            // TODO: Handle not logged
//        } else {
//            currentUser = request.getProfile();
//            // TODO: Handle logged
//        }
    }

//    private static void writeData(School school) {
//        try {
//            FileOutputStream saveFile = new FileOutputStream(path);
//            ObjectOutputStream save = new ObjectOutputStream(saveFile);
//            save.writeObject(school);
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//    }
//
//    private static School readData() {
//        School school = new School();
//        try {
//            FileInputStream readFile = new FileInputStream(path);
//            ObjectInputStream read = new ObjectInputStream(readFile);
//            school = (School) read.readObject();
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//        return school;
//    }
}
